﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using GeoHelper.API.Models;
using GeoHelper.Domain.Contracts;
using GeoHelper.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace GeoHelper.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MapController
    {
        private readonly IMapService _mapService;

        public MapController(IMapService mapService)
        {
            _mapService = mapService;
        }

        [HttpPost]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.UnprocessableEntity)]
        public void SetMap([FromBody]string mapInput)
        {
            if (!Map.TryParse(mapInput, out Map map))
            {
                throw ApiError.UnprocessableEntity("Could not parse the map");
            }

            _mapService.SetMap(map);
        }

        [HttpPost]
        [ProducesResponseType(typeof(IEnumerable<int>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public IEnumerable<int> CalculateLakeSquares([FromBody]Point[] points)
        {
            var map = _mapService.GetCurrentMap();
            if (map == null)
            {
                throw ApiError.ResourceNotFound("There's no active map. Please, SetMap before calling CalculateLakeSquares method");
            }

            return points.Select(p => _mapService.GetLakeArea(map, p));
        }
    }
}
