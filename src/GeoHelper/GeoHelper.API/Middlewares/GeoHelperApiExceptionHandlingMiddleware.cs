﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using GeoHelper.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;

namespace GeoHelper.API.Middlewares
{
    public class GeoHelperApiExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public GeoHelperApiExceptionHandlingMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context).ConfigureAwait(true);
            }
            catch (GeoHelperApiException ex)
            {
                var errorResponse = new GeoHelperApiErrorResponse
                {
                    Title = ReasonPhrases.GetReasonPhrase((int)ex.StatusCode),
                    StatusCode = (int)ex.StatusCode,
                    ErrorMessage = ex.Message
                };

                await WriteErrorResponse(context, errorResponse)
                    .ConfigureAwait(true);
            }
            catch (Exception ex)
            {
                var errorResponse = new GeoHelperApiErrorResponse
                {
                    Title = ReasonPhrases.GetReasonPhrase((int)HttpStatusCode.InternalServerError),
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    ErrorMessage = "Server Error"
                };

                await WriteErrorResponse(context, errorResponse)
                    .ConfigureAwait(true);
            }
        }
        
        private Task WriteErrorResponse(HttpContext context, GeoHelperApiErrorResponse errorResponse)
        {
            if (context.Response.HasStarted)
            {
                context.Abort();
                return Task.CompletedTask;
            }

            context.Response.StatusCode = errorResponse.StatusCode;
            context.Response.ContentType = "application/json; charset=utf-8";

            return context.Response.WriteAsync(GetResponseBody(errorResponse));
        }

        private static string GetResponseBody(GeoHelperApiErrorResponse errorResponse)
            => JsonConvert.SerializeObject(errorResponse);
    }
}
