﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace GeoHelper.API.Models
{
    [DataContract]
    public class GeoHelperApiErrorResponse
    {
        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "code")]
        public int StatusCode { get; set; }

        [DataMember(Name = "error_message")]
        public string ErrorMessage { get; set; }
    }
}
