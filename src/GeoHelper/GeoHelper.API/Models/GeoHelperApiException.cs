﻿using System;
using System.Net;

namespace GeoHelper.API.Models
{
    public class GeoHelperApiException : Exception
    {
        public HttpStatusCode StatusCode { get; }

        public GeoHelperApiException(HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
        }

        public GeoHelperApiException(HttpStatusCode subCode, string message)
            : base(message)
        {
            StatusCode = subCode;
        }
    }

    public static class ApiError
    {
        public static GeoHelperApiException UnprocessableEntity(string message = null)
            => new GeoHelperApiException(HttpStatusCode.UnprocessableEntity, message);

        public static GeoHelperApiException ResourceNotFound(string message = null)
            => new GeoHelperApiException(HttpStatusCode.NotFound, message);
    }
}
