﻿using GeoHelper.Domain;
using GeoHelper.Domain.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace GeoHelper.API.Infrastructure
{
    public class CompositionRoot
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IMapService, MapService>();
        }
    }
}
