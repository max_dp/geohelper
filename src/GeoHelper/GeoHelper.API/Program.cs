﻿using System;
using GeoHelper.API.Infrastructure;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace GeoHelper.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var compositionRoot = new CompositionRoot();
            CreateWebHostBuilder(args, compositionRoot.ConfigureServices).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args, Action<IServiceCollection> configureServices) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureServices(configureServices)
                .UseStartup<Startup>();
    }
}
