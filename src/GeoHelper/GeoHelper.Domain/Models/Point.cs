﻿using System.Runtime.Serialization;

namespace GeoHelper.Domain.Models
{
    [DataContract]
    public struct Point
    {
        [DataMember(Name = "x")]
        public int X { get; }

        [DataMember(Name = "y")]
        public int Y { get; }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
