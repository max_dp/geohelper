﻿using System;
using System.Linq;

namespace GeoHelper.Domain.Models
{
    public class Map
    {
        private CellTypeEnum[][] _matrix;

        public int Width { get; }

        public int Height { get; }

        public Map()
        {
            this.Width = 0;
            this.Height = 0;
        }

        private Map(CellTypeEnum[][] matrix)
        {
            _matrix = matrix;
            this.Height = matrix.Length;
            this.Width = matrix[0].Length;
        }

        public bool HasPoint(Point p)
        {
            return p.Y >= 0 && p.Y < Height
                && p.X >= 0 && p.X < Width;
        }

        public CellTypeEnum GetCellType(Point point)
        {
            if (!this.HasPoint(point))
            {
                throw new ArgumentOutOfRangeException(nameof(point));
            }

            return _matrix[point.Y][point.X];
        }

        public static bool TryParse(string input, out Map map)
        {
            map = default;
            if (!string.IsNullOrEmpty(input))
            {
                var lines = input.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                if (lines.Length > 0)
                {
                    var matrix = new CellTypeEnum[lines.Length][];
                    for (int i = 0; i < lines.Length; i++)
                    {
                        matrix[i] = new CellTypeEnum[lines[i].Length];
                        for (int j = 0; j < lines[i].Length; j++)
                        {
                            if (TryParseCellType(lines[i][j], out var cellType))
                            {
                                matrix[i][j] = cellType;
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }

                    bool isValidMatrix = IsValidMatrix(matrix);
                    if (isValidMatrix)
                    {
                        map = new Map(matrix);
                    }
                    return isValidMatrix;
                }
            }

            map = new Map();
            return true;
        }

        private static bool TryParseCellType(char input, out CellTypeEnum cellType)
        {
            cellType = default;
            switch (input)
            {
                case '#':
                    cellType = CellTypeEnum.Land;
                    return true;
                case 'O':
                    cellType = CellTypeEnum.Water;
                    return true;
                default:
                    return false;
            }
        }

        private static bool IsValidMatrix(CellTypeEnum[][] matrix)
        {
            return matrix != null
                && matrix.Length > 0
                && matrix.All(line => line.Length == matrix[0].Length);
        }
    }
}
