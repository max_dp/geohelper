﻿using System.Collections.Generic;
using GeoHelper.Domain.Contracts;
using GeoHelper.Domain.Models;

namespace GeoHelper.Domain
{
    public class MapService : IMapService
    {
        private Map _map;

        public void SetMap(Map map)
        {
            _map = map;
        }

        public Map GetCurrentMap()
        {
            return _map;
        }

        public int GetLakeArea(Map map, Point initialPoint)
        {
            var lakePoints = new List<Point>();
            var pointsToCheck = new Stack<Point>();
            pointsToCheck.Push(initialPoint);

            while (pointsToCheck.Count > 0)
            {
                Point point = pointsToCheck.Pop();
                if (map.HasPoint(point))
                {
                    if (map.GetCellType(point) == CellTypeEnum.Water && !lakePoints.Contains(point))
                    {
                        lakePoints.Add(point);
                        pointsToCheck.Push(new Point(point.X - 1, point.Y));
                        pointsToCheck.Push(new Point(point.X + 1, point.Y));
                        pointsToCheck.Push(new Point(point.X, point.Y - 1));
                        pointsToCheck.Push(new Point(point.X, point.Y + 1));
                    }
                }
            }

            return lakePoints.Count;
        }
    }
}
