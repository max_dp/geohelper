﻿using GeoHelper.Domain.Models;

namespace GeoHelper.Domain.Contracts
{
    public interface IMapService
    {
        void SetMap(Map map);

        Map GetCurrentMap();

        int GetLakeArea(Map map, Point p);
    }
}
