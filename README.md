# Geo Helper

GeoHelper allows to calculate a square of a lake where given point belongs.  
Navigate to `/swagger` page to get API description.

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/f714d5f7b36e7c85b028)

---

## The task

Your mission is to pinpoint the surface areas of water. You have a map which describes the contents of each square meter of a geographical zone. One square meter is composed of either land or water. One map can contain several bodies of water.  

Our program receives as input coordinates and the map. For each one you must determine the surface area of the lake which is located there. If there is no lake, then the surface area equals 0.  

The green squares represent land, and the white squares represent water. A lake is made up of a set of water squares which are horizontally or vertically adjacent. Two squares which are only diagonally adjacent are not part of the same lake.