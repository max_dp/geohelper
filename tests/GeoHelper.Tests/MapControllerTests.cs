﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using FluentAssertions;
using GeoHelper.API.Controllers;
using GeoHelper.API.Models;
using GeoHelper.Domain.Contracts;
using GeoHelper.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace GeoHelper.Tests
{
    [TestClass]
    public class MapControllerTests
    {
        private readonly MapController _sut;
        private readonly Mock<IMapService> _mapServiceMock = new Mock<IMapService>();
        
        public MapControllerTests()
        {
            _sut = new MapController(_mapServiceMock.Object);
        }

        [TestMethod]
        public void SetMap_ValidMapPassed_MapIsSet()
        {
            //Arrange
            string mapInput = GetValidMap();

            //Act
            _sut.SetMap(mapInput);

            //Assert
            _mapServiceMock.Verify(ms => ms.SetMap(It.IsAny<Map>()), Times.Once());
        }

        [TestMethod]
        public void SetMap_InvalidMapPassed_ExceptionIsThrown()
        {
            //Arrange
            Exception thrownException = default;
            string invalidMapInput = "12345";

            //Act
            try
            {
                _sut.SetMap(invalidMapInput);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }

            //Assert
            ValidateError(thrownException, HttpStatusCode.UnprocessableEntity);
            _mapServiceMock.Verify(ms => ms.SetMap(It.IsAny<Map>()), Times.Never());
        }

        [TestMethod]
        public void CalculateLakeSquares_MapIsSet_SqaresReturned()
        {
            //Arrange
            const int TEST_SQUARE = 5;
            var testPoints = GetTestPoints();
            var expected = Enumerable.Repeat(TEST_SQUARE, testPoints.Length);

            _mapServiceMock.Setup(ms => ms.GetCurrentMap()).Returns(new Map());
            _mapServiceMock.Setup(ms => ms.GetLakeArea(It.IsAny<Map>(), It.IsAny<Point>())).Returns(TEST_SQUARE);

            //Act
            var actual = _sut.CalculateLakeSquares(testPoints);

            //Assert
            ValidateSquares(expected, actual);
        }

        [TestMethod]
        public void CalculateLakeSquares_MapIsNotSet_ExceptionIsThrown()
        {
            //Arrange
            Exception thrownException = default;
            var testPoints = GetTestPoints();
            _mapServiceMock.Setup(ms => ms.GetCurrentMap()).Returns(null as Map);

            //Act
            try
            {
                var actual = _sut.CalculateLakeSquares(testPoints);
            }
            catch (Exception ex)
            {
                thrownException = ex;
            }

            //Assert
            ValidateError(thrownException, HttpStatusCode.NotFound);
            _mapServiceMock.Verify(ms => ms.SetMap(It.IsAny<Map>()), Times.Never());
        }        

        private void ValidateError(Exception ex, HttpStatusCode statusCode)
        {
            ex.Should()
                .NotBeNull()
                .And
                .BeOfType<GeoHelperApiException>();

            var apiException = ex as GeoHelperApiException;
            apiException.StatusCode.Should().Be(statusCode);
        }

        public void ValidateSquares(IEnumerable<int> expected, IEnumerable<int> actual)
        {
            actual.Should()
                .NotBeNull()
                .And
                .HaveCount(expected.Count())
                .And
                .BeEquivalentTo(expected);
        }

        private string GetValidMap() => "####\r\n##O#\r\n#OO#\r\n####";

        private Point[] GetTestPoints() => new Point[] { new Point(0, 0), new Point(2, 3) };
    }
}
