﻿using FluentAssertions;
using GeoHelper.Domain;
using GeoHelper.Domain.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GeoHelper.Tests
{
    [TestClass]
    public class MapServiceTests
    {
        private readonly MapService _sut;

        public MapServiceTests()
        {
            _sut = new MapService();
        }

        [TestMethod]
        public void GetLakeArea_WaterPoint_SquareReturned()
        {
            //Arrange
            var map = GetTestMap();
            var (waterPoint, expectedSquare) = GetWaterPoint();

            //Act
            int actual = _sut.GetLakeArea(map, waterPoint);

            //Assert
            actual.Should().Be(expectedSquare);
        }

        [TestMethod]
        public void GetLakeArea_LandPoint_ZeroReturned()
        {
            //Arrange
            var map = GetTestMap();
            var (landPoint, expectedSquare) = GetLandPoint();

            //Act
            int actual = _sut.GetLakeArea(map, landPoint);

            //Assert
            actual.Should().Be(expectedSquare);
        }

        [TestMethod]
        public void GetLakeArea_EmptyMap_ZeroReturned()
        {
            //Arrange
            var emptyMap = new Map();
            var anyPoint = GetWaterPoint().Point;
            
            //Act
            int actual = _sut.GetLakeArea(emptyMap, anyPoint);

            //Assert
            actual.Should().Be(0);
        }

        private string GetStringTestMap() => "####\r\n##O#\r\n#OO#\r\n####";

        private Map GetTestMap() => Map.TryParse(GetStringTestMap(), out Map map) ? map : null;
        
        private (Point Point, int ExpectedSquare) GetWaterPoint() => (new Point(2, 1), 3);

        private (Point Point, int ExpectedSquare) GetLandPoint() => (new Point(1, 1), 0);
    }
}
